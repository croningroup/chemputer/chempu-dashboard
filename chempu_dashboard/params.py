class DashboardParams:
    """Collection of static variables used by dashboard."""

    #: How far back in seconds to show on live chart
    live_time_range = 60 * 5

    #: How often in seconds to update the live chart
    update_interval = 2

    #: Max reading frequency in Hz
    max_freq = 2.4

    #: Min reading frequency in Hz
    min_freq = 0.1

    #: Default reading frequency in Hz
    default_freq = 0.5

    #: Step for choosing reading frequency with slider
    freq_step = 0.1

    #: Marks to show on frequency slider. Generated programmatically from above
    #: variables.
    slider_marks = {}

    def __init__(self):
        self._initialize_slider_marks()

    def _initialize_slider_marks(self):
        self.slider_marks = {0.1: "0.1 Hz"}
        i = 0.5
        while i <= self.max_freq:
            if float(i) == int(i):
                i = int(i)
            self.slider_marks[i] = f"{i:.1f} Hz"
            i += self.freq_step * 5
