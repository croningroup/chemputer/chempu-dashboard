def get_line_label(sensor_reader_name, reading_function):
    """Get label to show for line in chart."""
    return f"{sensor_reader_name}: {reading_function}"
