import datetime
import logging
import math
import threading
import time
from typing import TYPE_CHECKING, Dict

import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
import plotly.express as px
import plotly.graph_objects as go
from dash import Dash
from dash.dependencies import Input, Output

from .params import DashboardParams
from .state import DashboardState
from .utils import get_line_label

if TYPE_CHECKING:
    from chempiler.tools.sensor_reading.sensor_reader import SensorReader


class Dashboard:
    #: Spacing between multiple y axes on static chart
    y_axis_spacing = 0.04

    def __init__(self, chempiler=None):
        self.dash = Dash(
            title="ChemPU Dashboard",
            update_title="",  # Stop page title changing on every chart update
            external_stylesheets=[dbc.themes.FLATLY],
        )
        self.state = DashboardState()
        self.params = DashboardParams()

        # Don't show Flask output as it isn't helpful
        logging.getLogger("werkzeug").setLevel(logging.ERROR)

        if chempiler:
            self.bind_sensor_readers(chempiler.sensor_readers)

    #############
    # Lifecycle #
    #############

    def bind_sensor_readers(self, sensor_readers: Dict[str, "SensorReader"]):
        """Bind Chempiler sensor readers list to dashboard."""
        self.state.reset()
        self.state.sensor_readers = sensor_readers

        sensor_reader_names = []

        for sensor_reader, sensor_reader_obj in sensor_readers.items():
            self.state.sensor_frequencies[sensor_reader] = (
                1 / sensor_reader_obj.read_interval
            )
            self.state.start_btn_clicks[sensor_reader] = 0
            self.state.stop_btn_clicks[sensor_reader] = 0

            sensor_reader_names.append(sensor_reader)
            for reading_function in sensor_reader_obj.reading_functions:
                self.state.trace_titles.append(
                    get_line_label(sensor_reader, reading_function)
                )

        self.state.sorted_sensor_readers = sorted(sensor_reader_names)
        self.initialize_axes()
        self.initialize_layout()
        self.initialize_callbacks()

    def run(self, daemon=True, debug=True):
        """Run dashboard."""
        if daemon:
            # Debug not passed to `run_server` as it doesn't work inside a
            # thread.
            self.server_thread = threading.Thread(target=lambda: self.dash.run_server())
            self.server_thread.start()
        else:
            self.dash.run_server(debug=debug)

    ##########
    # Layout #
    ##########

    def initialize_axes(self):
        """Initialize variables used when generating axes."""
        self.state.n_traces = len(self.state.trace_titles)
        n_left_axes = (
            self.state.n_traces / 2
            if self.state.n_traces % 2 == 0
            else self.state.n_traces % 2 + self.state.n_traces // 2
        )
        n_right_axes = n_left_axes if self.state.n_traces % 2 == 0 else n_left_axes - 1
        self.state.axes.left_offset = self.y_axis_spacing * n_left_axes
        self.state.axes.right_offset = -self.y_axis_spacing * n_right_axes

    def initialize_layout(self):
        self.dash.layout = html.Div(
            children=[
                html.H1("ChemPU Dashboard", className="text-center"),
                self.control_panel(),
                html.Hr(),
                html.Div(children=[], style={"display": "flex"}),
                html.Div(
                    children=[
                        dcc.Graph(
                            id="live",
                        ),
                    ]
                ),
                html.Hr(),
                dcc.Graph(
                    id="static",
                ),
                html.Div(
                    children=[
                        html.Button(
                            "Refresh",
                            id="refresh-static-chart",
                            n_clicks=0,
                            className="btn btn-primary mb-3",
                        ),
                    ],
                    className="d-flex justify-content-center",
                ),
                dcc.Interval(
                    id="interval-component",
                    interval=self.params.update_interval * 1000,
                    n_intervals=0,
                ),
                html.Div(id="slider-hidden-div", style={"display": "none"}),
                html.Div(id="start-btn-hidden-div", style={"display": "none"}),
                html.Div(id="stop-btn-hidden-div", style={"display": "none"}),
                html.Div(id="live-legend-hidden-div", style={"display": "none"}),
                html.Div(id="static-legend-hidden-div", style={"display": "none"}),
            ],
            className="p-4",
        )

    def sensor_controls(self, sensor_reader):
        sensor_name = sensor_reader.sensor_name
        return html.Div(
            children=[
                html.H2(sensor_name, style={"font-size": 18, "text-align": "center"}),
                html.Div(
                    children=[
                        html.Button(
                            "Stop",
                            id=f"stop-btn-{sensor_name}",
                            n_clicks=0,
                            className="btn btn-secondary m-2 mb-3",
                        ),
                        html.Button(
                            "Start",
                            id=f"start-btn-{sensor_name}",
                            n_clicks=0,
                            className="btn btn-secondary m-2 mb-3",
                        ),
                    ],
                    style={"display": "flex", "justify-content": "center"},
                ),
                dcc.Slider(
                    id=f"slider-{sensor_name}",
                    min=self.params.min_freq,
                    max=self.params.max_freq,
                    step=self.params.freq_step,
                    value=self.params.default_freq,
                    marks=self.params.slider_marks,
                    included=False,
                ),
            ],
            className="border m-2 p-2",
            style={"width": "300px"},
        )

    def control_panel(self):
        return html.Div(
            children=[
                *[
                    self.sensor_controls(self.state.sensor_readers[sensor_reader])
                    for sensor_reader in self.state.sorted_sensor_readers
                ],
            ],
            className="m-5 d-flex justify-content-center flex-wrap",
        )

    ###########
    # Figures #
    ###########

    def make_live_fig(self):
        sensor_readers = self.state.sensor_readers
        live_fig = go.Figure(layout={"title": {"text": "Live Readings"}})
        live_fig.update_yaxes(fixedrange=True)
        live_fig.update_xaxes(fixedrange=True)
        trace_idx = 0
        for item in self.state.sorted_sensor_readers:
            sensor_reader = sensor_readers[item]
            live_start_time = time.time() - self.params.live_time_range
            i = len(sensor_reader.time) - 1
            time_range_slice = slice(0, len(sensor_reader.time))
            while i >= 0:
                if sensor_reader.time[i] < live_start_time:
                    time_range_slice = slice(i + 1, len(sensor_reader.time))
                    break
                i -= 1

            for reading_function in sensor_reader.reading_functions:
                name = self.state.trace_titles[trace_idx]
                live_fig.add_trace(
                    go.Scattergl(
                        x=[
                            datetime.datetime.fromtimestamp(val)
                            for val in sensor_reader["time"][time_range_slice]
                        ],
                        # transforming back into a list here
                        # (because it's a deque in chempiler now)
                        y=list(sensor_reader[reading_function])[time_range_slice],
                        mode="lines+markers",
                        uid=name,
                        name=name,
                        visible=self.state.visible_live_traces.get(trace_idx, True),
                        yaxis=f"y{trace_idx + 1}" if trace_idx > 0 else "y",
                    )
                )
                trace_idx += 1

        self.add_multiple_y_axes(live_fig)

        live_fig.update_layout(
            legend={
                "orientation": "h",
                "yanchor": "bottom",
                "y": 1.02,
                "xanchor": "right",
                "x": 1,
            }
        )
        return live_fig

    def make_static_fig(self):
        sensor_readers = self.state.sensor_readers
        static_fig = go.Figure(layout={"title": {"text": "Static Readings"}})
        static_fig.update_yaxes(fixedrange=True)
        n_traces = 0
        names = []
        for item in self.state.sorted_sensor_readers:
            sensor_reader = sensor_readers[item]
            for reading_function in sensor_reader.reading_functions:
                readings = list(sensor_reader[reading_function])
                name = get_line_label(item, reading_function)
                names.append(name)
                static_fig.add_trace(
                    go.Scattergl(
                        x=[
                            datetime.datetime.fromtimestamp(val)
                            for val in sensor_reader.time
                        ],
                        y=readings,
                        mode="lines+markers",
                        uid=name,
                        name=name,
                        visible=self.state.visible_static_traces.get(n_traces, True),
                        yaxis=f"y{n_traces + 1}" if n_traces > 0 else "y",
                    )
                )
                n_traces += 1

        self.add_multiple_y_axes(static_fig)

        static_fig.update_layout(
            legend={
                "orientation": "h",
                "yanchor": "bottom",
                "y": 1.02,
                "xanchor": "right",
                "x": 1,
            }
        )

        return static_fig

    def add_multiple_y_axes(self, fig):
        layout = {}
        for i in range(self.state.n_traces):
            color = px.colors.qualitative.Plotly[i % len(px.colors.qualitative.Plotly)]
            yaxis_props = {
                "anchor": "free",
                "tickfont": {"color": color},
                "titlefont": {"color": color},
            }
            if i == 0:
                ykey = "yaxis"
                yaxis_props.update(
                    {
                        "side": "left",
                        "position": self.state.axes.left_offset,
                    }
                )
                layout[ykey] = yaxis_props

            else:
                ykey = f"yaxis{i + 1}"

                # Left
                if i % 2 == 0:
                    yaxis_props.update(
                        {
                            "overlaying": f"y{i + 1}",
                            "side": "left",
                            "position": (
                                self.state.axes.left_offset
                                - (math.floor(i / 2) * self.y_axis_spacing)
                            ),
                        }
                    )
                    layout[ykey] = yaxis_props

                else:
                    yaxis_props.update(
                        {
                            "overlaying": f"y{i + 1}",
                            "side": "right",
                            "position": (
                                1
                                + self.state.axes.right_offset
                                + (math.floor(i / 2) * self.y_axis_spacing)
                            ),
                        }
                    )
                    layout[ykey] = yaxis_props

        layout["xaxis"] = {
            "domain": (self.state.axes.left_offset, 1 + self.state.axes.right_offset)
        }
        fig.update_layout(layout)

    #############
    # Callbacks #
    #############

    def on_live_chart_legend_click(self, update):
        """Update which traces are visible on live chart."""
        if update is None:
            return
        update_type = update[0]["visible"][0]
        trace_index = update[1][0]
        self.state.visible_live_traces[trace_index] = update_type

    def on_static_chart_legend_click(self, update):
        """Update which traces are visible on static chart."""
        if update is None:
            return
        update_type = update[0]["visible"][0]
        trace_index = update[1][0]
        self.state.visible_static_traces[trace_index] = update_type

    def on_slider_update(self, *args):
        for i, sensor_reader in enumerate(self.state.sorted_sensor_readers):
            freq = args[i]
            if freq != self.state.sensor_frequencies[sensor_reader]:
                self.state.sensor_frequencies[sensor_reader] = freq
                self.state.sensor_readers[sensor_reader].change_reading_interval(
                    1 / freq
                )

        return ""

    def on_start_btn_update(self, *args):
        for i, sensor_reader_name in enumerate(self.state.sorted_sensor_readers):
            if args[i] > self.state.start_btn_clicks[sensor_reader_name]:
                self.state.start_btn_clicks[sensor_reader_name] = args[i]
                sensor_reader = self.state.sensor_readers[sensor_reader_name]
                if sensor_reader._reading_thread is None:
                    sensor_reader.start()
                else:
                    sensor_reader.resume()

        return ""

    def on_stop_btn_update(self, *args):
        for i, sensor_reader_name in enumerate(self.state.sorted_sensor_readers):
            if args[i] > self.state.stop_btn_clicks[sensor_reader_name]:
                self.state.stop_btn_clicks[sensor_reader_name] = args[i]
                sensor_reader = self.state.sensor_readers[sensor_reader_name]
                sensor_reader.pause()

        return ""

    def on_live_chart_update(self, n):
        return self.make_live_fig()

    def on_static_chart_update(self, n_clicks):
        return self.make_static_fig()

    def initialize_callbacks(self):
        # Live chart update
        self.dash.callback(
            Output("live", "figure"),
            Input("interval-component", "n_intervals"),
        )(self.on_live_chart_update)

        # Static chart update
        self.dash.callback(
            Output("static", "figure"),
            Input("refresh-static-chart", "n_clicks"),
        )(self.on_static_chart_update)

        # Change sensor reading frequency sliders
        self.dash.callback(
            Output("slider-hidden-div", "children"),
            *[
                Input(f"slider-{sensor_name}", "value")
                for sensor_name in self.state.sorted_sensor_readers
            ],
        )(self.on_slider_update)

        # Start sensor reading buttons
        self.dash.callback(
            Output("start-btn-hidden-div", "children"),
            *[
                Input(f"start-btn-{sensor_name}", "n_clicks")
                for sensor_name in self.state.sorted_sensor_readers
            ],
        )(self.on_start_btn_update)

        # Stop sensor reading buttons
        self.dash.callback(
            Output("stop-btn-hidden-div", "children"),
            *[
                Input(f"stop-btn-{sensor_name}", "n_clicks")
                for sensor_name in self.state.sorted_sensor_readers
            ],
        )(self.on_stop_btn_update)

        # Live chart legend trace selection
        self.dash.callback(
            Output("live-legend-hidden-div", "children"),
            *[Input("live", "restyleData")],
        )(self.on_live_chart_legend_click)

        # Static chart legend trace selection
        self.dash.callback(
            Output("static-legend-hidden-div", "children"),
            *[Input("static", "restyleData")],
        )(self.on_static_chart_legend_click)
