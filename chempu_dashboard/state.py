class AxesState:
    def __init__(self):
        self.reset()

    def reset(self):
        self.left_offset = 0
        self.right_offset = 0

    #: Number between 0 and 1 representing how much of available space left
    #: y axes take up.
    left_offset: float = 0

    #: Number between 0 and 1 representing how much of available space right
    #: y axes take up.
    right_offset: float = 0


class DashboardState:
    def __init__(self):
        # Good to do this so every instance has its own objects rather than a
        # ref to the ones attached to the class.
        self.reset()

    def reset(self):
        self.sensor_frequencies = {}
        self.sensor_readers = {}
        self.trace_titles = []
        self.n_traces = None
        self.sorted_sensor_readers = []
        self.start_btn_clicks = {}
        self.stop_btn_clicks = {}
        self.visible_live_traces = {}
        self.visible_static_traces = {}
        self.axes = AxesState()

    #: Frequency at which every sensor reader is currently set to.
    #: { sensor_name: read_frequency_hz... }
    sensor_frequencies = {}

    #: All sensor readers in format { sensor_name: sensor_reader_obj... }
    sensor_readers = {}

    #: Alphabetically sorted list of all sensor reader names.
    sorted_sensor_readers = []

    #: Number of Start btn clicks for each sensor reader. Used to track which
    #: button has been clicked in callback.
    #: { sensor_reader_name: number_of_start_button_clicks }
    start_btn_clicks = {}

    #: Number of Stop btn clicks for each sensor reader. Used to track which
    #: button has been clicked in callback.
    #: { sensor_reader_name: number_of_stop_button_clicks }
    stop_btn_clicks = {}

    #: Dict of which traces are visible on the live chart. Keys are trace
    #: indexes, values are ``True`` or ``"legendonly"``.
    visible_live_traces = {}

    #: Dict of which traces are visible on the static chart. Keys are trace
    #: indexes, values are ``True`` or ``"legendonly"``.
    visible_static_traces = {}

    #: Layout details of y axes
    axes = AxesState()
