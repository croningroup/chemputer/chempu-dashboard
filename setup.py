from setuptools import find_packages, setup

setup(
    name="chempu-dashboard",
    version="1.0.0",
    description="Chempu Dashboard",
    url="https://gitlab.com/croningroup/chemputer/chempu-dashboard",
    author="Matthew Craven & Artem Leonov",
    packages=find_packages(),
    install_requires=[
        "werkzeug~=2.0.0",
        "dash~=1.20.0",
        "dash-bootstrap-components~=0.12.0",
        "pandas~=2.0",  # Needed for plotly express
    ],
    zip_safe=False,
)
