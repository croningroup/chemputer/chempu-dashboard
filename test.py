import ChemputerAPI
from chempiler import Chempiler

from chempu_dashboard import Dashboard

c = Chempiler(
    graph_file="tests/files/test_graph.json",
    simulation=True,
    experiment_code="test",
    output_dir=".",
    device_modules=[ChemputerAPI],
)

dashboard = Dashboard(c)
dashboard.run()
