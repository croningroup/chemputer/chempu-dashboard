import os

import ChemputerAPI
from chempiler import Chempiler

from chempu_dashboard import Dashboard

HERE = os.path.abspath(os.path.dirname(__file__))


def test_basic():
    c = Chempiler(
        experiment_code="test",
        output_dir=os.path.join(HERE, "test_output"),
        device_modules=[ChemputerAPI],
        simulation=True,
        graph_file=os.path.join(HERE, "files", "test_graph.json"),
    )

    dashboard = Dashboard()
    dashboard.bind_sensor_readers(c.sensor_readers)
