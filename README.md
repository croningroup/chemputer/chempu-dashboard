# ChemPU Dashboard

Dashboard for monitoring and visualizing ChemPU sensor data.

## Installation

`pip install -e chempu-dashboard`

## Usage

The following will run the dashboard in a background thread on [localhost:8050](http://localhost:8050).

```
from chempiler import Chempiler
from chempu_dashboard import Dashboard

c = Chempiler(...)

dashboard = Dashboard(c)
dashboard.run()
```
